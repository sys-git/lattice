"""
"""
import abc
import itertools
import uuid
from collections import namedtuple

_serial_sentinel = object()
NO_RESULT = object()
NO_DATA = object()

cfg_options = dict(
    on_grid=True,  # Cannot run grid on grid unless max-grid-depth is enabled.
    grid_config={},  # Config to configure the grid with.
    abort_on_error=True,
    # Raise a PipelineTaskError if an error occurs on the grid, unless ``abort_on_grid_error`` is True.
    abort_on_grid_error=True,  # Let the grid raise this exception natively.
    ignore_grid_exceptions=[],
    # List of exceptions to ignore on the grid (not raise an error).
)

TaskResult = namedtuple('TaskResult', ('result', 'is_error'))


def parallel_apply(func, iterable, args=None, kwargs=None, **imap_cfg):
    args = args or []
    kwargs = kwargs or {}
    return [func(i, *args, **kwargs) for i in iterable]


def _grid_worker(f, data, cfg, *args, **kwargs):
    try:
        result = f(data, cfg, *args, **kwargs)
    except cfg.get('ignore_grid_exceptions', None) as result:
        error = True
    except Exception as result:
        error = True
        if cfg.get('abort_on_grid_error', True):
            raise
    else:
        error = False

    return TaskResult(result, is_error=error)


def chunked_grid_worker(iterable, cfg, *args, **kwargs):
    """
    Runs on the grid.

    :param iterable: Data to apply to the func. Iterable is always a tuple of length=2.
    :param cfg: Config
    :type cfg: dict
    :return: A list (length=1)
    :rtype: TaskResult
    """
    task, data = iterable
    return _grid_worker(task, data, cfg, *args, **kwargs)
    # return _grid_worker(task, data, cfg) for task, data in iterable


#

def grid_worker(iterable, func, cfg, *args, **kwargs):
    """
    Runs on the grid.

    :param iterable: Data to apply to the func.
    :type iterable: iterable
    :param func: Function to apply the iterable to
    :type func: callable
    :param cfg: Config
    :type cfg: dict
    :return: The same number of results as ``iterable``s.
    :rtype: iterable(TaskResult)
    """
    return _grid_worker(func, iterable, cfg, *args, **kwargs)


def imap(func, iterable, args, kwargs=None, **imap_cfg):
    """
    Parallelise the execution of an iterable against a func.

    Like map but on a grid.

    :return: an iterable representing the results of each iterable's execution
    in func.
    """
    return parallel_apply(func, iterable, args=args, kwargs=kwargs, **imap_cfg)


class PipelineTaskError(Exception):
    pass


class Task(object):
    def __init__(self, name=None, data=None, cfg=None):
        self._task_id = uuid.uuid4()
        self._result = NO_RESULT
        self._name = name or self._task_id.hex
        self._cfg = cfg or {}
        self._data = data

    @property
    def name(self):
        return self._name

    @property
    def cfg(self):
        return self._cfg

    @property
    def task_id(self):
        return self._task_id

    @property
    def data(self):
        return self._data

    @property
    def has_result(self):
        return self._result is not NO_RESULT

    @property
    def result(self):
        return self._result

    @result.setter
    def result(self, r):
        self._result = r

    def __call__(self, *args, **kwargs):
        return self.run(*args, **kwargs)


class _Pipeline(Task):
    def __init__(self, name=None, tasks=frozenset(), data=None, cfg=None):
        super(_Pipeline, self).__init__(name=name, data=data, cfg=cfg)
        self._tasks = tasks or []

    @property
    def tasks(self):
        return self._tasks

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        # execute jobs
        result = self.run()
        self.result = result

    def _run_on_grid(self, task, data, grid_config):
        return self._unbox(
            imap(chunked_grid_worker, self._box(task, data), args=(self.cfg,),
                 **grid_config))

    def _run_off_grid(self, task, data, cfg):
        return self._unbox(chunked_grid_worker(self._box(task, data), cfg))

    def _run(self, tasks, data, cfg):
        if cfg.get('on_grid', False):
            result = self._run_on_grid(tasks, data, cfg)
        else:
            result = self._run_off_grid(tasks, data, cfg)

        if result.is_error and cfg.get('abort_on_error', True):
            raise PipelineTaskError(result)

        return result.result

    @abc.abstractproperty
    def _box(self, task, data):
        """
        box data

        :param task: Thing to box up with data.
        :param data: Data to box up with task.
        :type data: iterable
        :return: boxde data.
        """
        pass

    @abc.abstractproperty
    def _unbox(self, result):
        """
        Unbox data

        :param result: result to unbox
        :return: unboxed data
        """

    def run(self, data=None, cfg=None):
        """
        Override as required.

        :param data: override data to use.
        :param cfg: override cfg to use.
        :rtype: generator/iterator of results.
        """
        return self._run(self.tasks, data or self.data, cfg or self.cfg)


class Grid(_Pipeline):
    """
    An abstraction around the usual grid interface, takes care of returning
    the correct number of results.
    """

    def __init__(self, name, func, data=None, cfg=None, args=None, kwargs=None):
        cfg = cfg or {}
        cfg.setdefault('on_grid', True)
        super(Grid, self).__init__(name=name, tasks=func, data=data, cfg=cfg)
        self._args = args
        self._kwargs = kwargs

    def _box(self, task, data):
        raise NotImplemented

    def _unbox(self, results):
        return TaskResult(itertools.chain([i.result for i in results]),
                          is_error=any([i.is_error for i in results]))

    def _run(self, func, data, cfg):
        args = [func, cfg]
        args.extend(self._args or [])

        result = self._unbox(
            imap(grid_worker, data, args=args, kwargs=self._kwargs, **cfg))

        if result.is_error and cfg.get('abort_on_error', True):
            raise PipelineTaskError(result)

        return result.result


class Serial(_Pipeline):
    """
    Feed the result from one task into the data of the subsequent task
    """

    def _box(self, task, data):
        return [(task, data)]

    def _unbox(self, result):
        return result[0]

    def run(self, data=None, cfg=None):
        data = data or self.data
        cfg = cfg or self.cfg

        for task in self.tasks:
            result = self._run(task, data, cfg)
            task.result = result.result
            data = task.result

        return result


class Parallel(_Pipeline):
    """
    Execute each job in parallel, passing the result from one task to the next
    if required by the tasks's config.
    """

    def __init__(self, name, tasks=frozenset(), data=None, weight='job',
                 cfg=None):
        super(Parallel, self).__init__(name=name, tasks=tasks, data=data,
                                       cfg=cfg)
        self._weight = weight

    def _box(self, task, data):
        # TODO: Break up the tasks/data into an iterable based on 'self.weight'
        return [(task, data)]

    def _unbox(self, result):
        # TODO: Merge the iterable results back into a task results, assign
        # the task results to each task and return the merge result.
        return result

    def run(self, data=None, cfg=None):
        return self._run_on_grid(self.tasks, data or self.data, cfg or self.cfg)


class Job(Task):
    def __init__(self, func, *args, **kwargs):
        super(Job, self).__init__(name=kwargs.pop('name', None),
                                  data=kwargs.pop('data', None),
                                  cfg=kwargs.pop('cfg', None))
        self._func = func
        self._args = args
        self._kwargs = kwargs

    def run(self, data, cfg=None):
        accept_data = cfg.get('accept_data', True)

        if accept_data is True:
            data = data
        elif accept_data is False:
            data = self.data
        else:
            data = data if data is not None else self.data

        try:
            result = self._func(data, *self._args, **self._kwargs)
        except Exception as result:
            error = True
        else:
            error = False

        result = TaskResult(result, is_error=error)
        self.result = result

        return result


class Pipeline(object):
    def __init__(self, name='pipeline', **config):
        self._name = name
        self._config = config

    @property
    def name(self):
        return self._name


def parse(data, *args, **kwargs):
    print 'parse {data}, {args}, {kwargs}'.format(data, args, kwargs)


def compress(data, *args, **kwargs):
    print 'compress {data}, {args}, {kwargs}'.format(data, args, kwargs)


def render(data, *args, **kwargs):
    print 'render {data}, {args}, {kwargs}'.format(data, args, kwargs)


def accumulate(data, *args, **kwargs):
    print 'accumulate {data}, {args}, {kwargs}'.format(data, args, kwargs)


def test_job():
    def f(data, *args, **kwargs):
        print 'f {data}, {args}, {kwargs}'.format(data=str(data), args=args,
                                                  kwargs=kwargs)
        assert args == ('arg1', 'arg2')
        assert kwargs == dict(kwargs1=1, kwarg2=2)

        return '.'.join([str(data), 'f'])

    def new_job():
        job = Job(f, 'arg1', 'arg2', name='j', data=123, cfg={}, kwargs1=1,
                  kwarg2=2)
        assert job.data == 123
        assert job.name
        assert job.cfg == {}
        assert job.task_id
        assert not job.has_result
        assert job.result == NO_RESULT
        return job

    j = new_job()
    result = j(None, {'accept_data': True})
    assert result == TaskResult('None.f', False) == j.result
    assert j.has_result

    j = new_job()
    result = j(456, {'accept_data': True})
    assert result == TaskResult('456.f', False) == j.result
    assert j.has_result

    j = new_job()
    result = j(None, {'accept_data': False})
    assert result == TaskResult('123.f', False) == j.result
    assert j.has_result

    j = new_job()
    result = j(456, {'accept_data': False})
    assert result == TaskResult('123.f', False) == j.result
    assert j.has_result

    j = new_job()
    result = j(None, {'accept_data': 'maybe'})
    assert result == TaskResult('123.f', False) == j.result
    assert j.has_result

    j = new_job()
    result = j(456, {'accept_data': 'maybe'})
    assert result == TaskResult('456.f', False) == j.result
    assert j.has_result

    # j = Job(f, 'arg1', 'arg2', name='j', data=NO_DATA, cfg={}, kwargs1=1, kwarg2=2)
    # result = j(None)
    # assert result == TaskResult('None.f', False)

    # j = Job(f, 'arg1', 'arg2', name='j', data=123, cfg={}, kwargs1=1, kwarg2=2)
    # result = j(None)
    # assert result == TaskResult('123.f', False)
    #
    # j = Job(f, 'arg1', 'arg2', name='j', data=123, cfg={}, kwargs1=1, kwarg2=2)
    # result = j(456)
    # assert result == TaskResult('456.f', False)


def test_chunked_grid_worker():
    config = {}

    def a(data, cfg):
        assert data == 1
        assert cfg == config
        return 111

    def b(data, cfg):
        assert data == 2
        assert cfg == config
        return 222

    iterable = [[(a, 1), (b, 2)]]
    result = chunked_grid_worker(iterable, config)
    assert result == [TaskResult(111, False), TaskResult(222, False)]


def test_grid_worker():
    i = []
    config = {'abort_on_grid_error': False}
    eData = 'blah'
    tmp = dict(count=0)

    class MyException(Exception):
        pass

    def f(data, cfg):
        assert data == eData
        assert cfg == config
        return 456

    def e(data, cfg):
        assert data == eData
        assert cfg == config
        raise MyException(789)

    def g(data, cfg):
        assert data == eData
        assert cfg == config
        tmp['count'] += 1
        if tmp['count'] % 2:
            raise MyException(789)
        return 765

    assert len(grid_worker(i, f, config)) == 0

    assert grid_worker([eData], f, config) == [TaskResult(456, False)]

    result = grid_worker([eData], e, config)
    assert len(result) == 1
    assert isinstance(result[0], TaskResult)
    assert isinstance(result[0].result, MyException)
    assert result[0].is_error

    result = grid_worker([eData, eData, eData], e, config)
    assert len(result) == 3
    assert all([isinstance(i.result, MyException) for i in result])
    result = grid_worker([eData, eData, eData], f, config)
    assert all([not isinstance(i.result, MyException) for i in result])
    assert len(result) == 3
    result = grid_worker([eData, eData, eData], g, config)
    assert len(result) == 3
    assert isinstance(result[0].result, MyException)
    assert result[1].result == 765
    assert isinstance(result[2].result, MyException)

    config = {'abort_on_grid_error': True}
    try:
        grid_worker([eData], e, config)
    except MyException:
        assert True
    else:
        assert False

    try:
        grid_worker([eData, eData], g, config)
    except MyException:
        assert True
    else:
        assert False

    config = {'ignore_grid_exceptions': MyException}
    result = grid_worker([eData, eData], g, config)
    assert len(result) == 2
    assert not isinstance(result[0].result, MyException)
    assert isinstance(result[1].result, MyException)
    assert not result[0].is_error
    assert result[1].is_error
    assert result[0].result == 765


def test_pipeline_off_grid():
    new_data = 'voo'

    name = 'hello'
    data = 'blah'
    args = (1, 2, 3)
    kwargs = dict(c=4, d=5)
    cfg = {'on_grid': False}

    def e(data_, *args_, **kwargs_):
        assert data_ == data
        assert args_ == args
        assert kwargs_ == kwargs
        return '.'.join(['e', data_])

    def f(data_, *args_, **kwargs_):
        assert data_ == new_data
        assert args_ == args
        assert kwargs_ == kwargs
        return '.'.join(['f', data_])

    tasks = Job(f, *args, **kwargs)

    class TstPipeline(_Pipeline):
        def _box(self, task, data):
            return (task, data)

        def _unbox(self, result):
            return result[0]

    t = TstPipeline(name=name, tasks=tasks, data=data, cfg=cfg)
    assert t.name == name
    assert t.tasks == tasks
    assert t.data == data
    assert t.cfg == cfg
    result = t(new_data)
    assert result == 'f.voo'

    tasks = Job(e, *args, **kwargs)
    t1 = TstPipeline(name=name, tasks=tasks, data=data, cfg=cfg)
    result = t1()
    assert result == 'e.blah'


def test_pipeline_ctx_manager():
    name = 'hello'
    data = 'blah'
    args = (1, 2, 3)
    kwargs = dict(c=4, d=5)
    cfg = {'on_grid': True}

    class TstPipeline(_Pipeline):
        def _box(self, task, data):
            return [(task, data)]

        def _unbox(self, result):
            return result[0]

    def f(data_, *args_, **kwargs_):
        assert data_ == data
        assert args_ == args
        assert kwargs_ == kwargs
        return '.'.join(['f', data_])

    tasks = Job(f, *args, **kwargs)

    x = TstPipeline(name=name, tasks=[tasks], data=data, cfg=cfg)
    with x:
        assert True
    result = x.result
    assert result == TaskResult('f.blah', False)


def test_pipeline_on_grid():
    new_data = 'voo'

    name = 'hello'
    data = 'blah'
    args = (1, 2, 3)
    kwargs = dict(c=4, d=5)
    cfg = {'on_grid': True}

    def e(data_, *args_, **kwargs_):
        assert data_ == data
        assert args_ == args
        assert kwargs_ == kwargs
        return '.'.join(['e', data_])

    def f(data_, *args_, **kwargs_):
        assert data_ == new_data
        assert args_ == args
        assert kwargs_ == kwargs
        return '.'.join(['f', data_])

    tasks = Job(f, *args, **kwargs)

    class TstPipeline(_Pipeline):
        def _box(self, task, data):
            return [(task, data)]

        def _unbox(self, result):
            return result[0]

    t = TstPipeline(name=name, tasks=tasks, data=data, cfg=cfg)
    assert t.name == name
    assert t.tasks == tasks
    assert t.data == data
    assert t.cfg == cfg
    result = t(new_data)
    assert result == TaskResult('f.voo', False)

    tasks = Job(e, *args, **kwargs)
    t1 = TstPipeline(name=name, tasks=tasks, data=data, cfg=cfg)
    result = t1()
    assert result == TaskResult('e.blah', False)


def test_grid():
    name = 'grid-name'
    data = [456, 789]
    cfg = {}

    def f(data_, cfg_, *args, **kwargs):
        assert data_ in data
        assert cfg_['on_grid']
        return data_ + 1

    g = Grid(name, f, data, cfg)
    result = g()
    result = list(result)
    assert result == [457, 790]


if __name__ == '__main__':
    # test_grid()
    test_pipeline_ctx_manager()
    # test_pipeline_on_grid()
    # test_pipeline_off_grid()
    # test_chunked_grid_worker()
    # test_grid_worker()
    # test_job()

    # cfg = {}
    #
    # job1 = Job(parse, args=('arg_1',), kwargs={1: 1}, data='data_1')
    # job2 = Job(parse, args=('arg_2',), kwargs={2: 2}, data='data_2')
    # job3 = Job(parse, args=('arg_3',), kwargs={3: 3}, data='data_3')
    # job4 = Job(compress, args=('arg_4',), kwargs={4: 4}, data='data_4')
    # job5 = Job(compress, args=('arg_5',), kwargs={5: 5}, data='data_5')
    # job6 = Job(compress, args=('arg_6',), kwargs={6: 6}, data='data_6')
    # job7 = Job(render, args=('arg_7',), kwargs={7: 7}, data='data_7')
    # job8 = Job(render, args=('arg_8',), kwargs={8: 8}, data='data_8')
    # job9 = Job(accumulate, args=('arg_9',), kwargs={6: 9}, data='data_9')
    #
    # '''
    # 'decode' runs 3 jobs in series, off-grid. The first job receives
    #     'job1_data_override' as input data.
    #
    # 'compress' runs 3 jobs in parallel, all on the grid, using 'render's cfg.
    # '''
    # item = Serial('pipeline', [
    #     Serial('decode', [job1, job2, job3],
    #            data='job1_data_if_pipeline_data_not_available'),
    #     Parallel('compress', [job4, job5, job6]),
    #     Serial('encode', [job7, job8, Parallel('export', [job9],
    #                                            weight='weight', cfg=cfg)])],
    #               data='job1_data')
    #
    # # Execute the pipeline:
    # result = item()
    # print result

    #
    # prep = Job('prep-data')
    # with Pipeline('task-q', jobs=[prep], **cfg) as s:
    #     s += Job('parse-data', parse)
    #     s += Job('render-data', render)
    # result = s[1].result
    #
    # '''
    # parse (non-grid) result is fed into
    # render (on-grid) whose result is fed into
    # accumulate (on-grid) whose result is made available
    # '''
    # result = Pipeline('task-q', accumulate,
    #                   Pipeline('render-data', render,
    #                            Pipeline('parse-data', parse),
    #                            *cfg), **cfg)()
    #
    # Parallel(name, jobs, hugsConfig)
